﻿using Input;
using UnityEngine;
using PlayerInput = UnityEngine.InputSystem.PlayerInput;

public class ReadInputPlayer : MonoBehaviour
{
    [SerializeField] private float _sensitivity;

    private GameplayInput _playerInputActions;
    private Vector2 _movementInput;

    private float _interactValue;

    public float InteractValue
    {
        get { return _interactValue; }
    }

    public Vector2 MovementAxis
    {
        get { return _movementInput; }
    }

    private void OnEnable()
    {
        _playerInputActions.Enable();
    }

    private void OnDisable()
    {
        _playerInputActions.Disable();
    }

    private void Awake()
    {
        _playerInputActions = new GameplayInput();

        // _playerInputActions.Gameplay.Interact.performed += ctx => this._interactValue = ctx.ReadValue<float>();
        // _playerInputActions.Gameplay.Interact.canceled += ctx => this._interactValue = 0.0f;

        _playerInputActions.Gameplay.Movement.performed += ctx => this._movementInput = ctx.ReadValue<Vector2>();
        _playerInputActions.Gameplay.Movement.canceled += ctx => this._movementInput = Vector2.zero;

        // _playerInputActions.Player.Look.performed += ctx => this._inputRotationAxis = ctx.ReadValue<Vector2>();
        // _playerInputActions.Player.Look.canceled += ctx => this._inputRotationAxis = Vector2.zero;
    }
}