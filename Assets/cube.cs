﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class cube : MonoBehaviour
{
    [SerializeField] private ReadInputPlayer _inputPlayer;
    public float rotationSpeed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(_inputPlayer.MovementAxis);
        transform.position += new Vector3(_inputPlayer.MovementAxis.x, _inputPlayer.MovementAxis.y, 0) * Time.deltaTime;
        // _inputPlayer.Horizontal;
        float rotation = Time.deltaTime * rotationSpeed;
        transform.Rotate(Vector3.up, rotation); 
    }
}
